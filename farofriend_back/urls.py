from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from core import views, viewsets

router = routers.DefaultRouter()
router.register(r'users', viewsets.UserViewSet)
router.register(r'groups', viewsets.GroupViewSet)
router.register(r'animal', viewsets.AnimalViewSet)
router.register(r'dog', viewsets.DogViewSet)
router.register(r'cat', viewsets.CatViewSet)
router.register(r'profile', viewsets.ProfileViewSet)
router.register(r'adopter', viewsets.AdopterViewSet)
# router.register(r'message', viewsets.MessageViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/messages', views.MessageView.as_view()),
    path('api/adoption_animals', views.ListAdpotionAnimalsView.as_view())
]