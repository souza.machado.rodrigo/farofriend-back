from django.contrib.auth.models import User, Group
from rest_framework import serializers

from core.models import Animal, Dog, Cat, Profile, Adopter, ChatMessage

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']

class AnimalSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Animal
        fields = ['name', 'breed', 'age', 'gender', 'fur_color', 'castrated', 'description', 'deficiencies']

class DogSerializer(AnimalSerializer):
    class Meta:
        model = Dog
        fields = ['name', 'breed', 'age', 'gender', 'fur_color', 'castrated', 'description', 'deficiencies', 'size', 'test_distemper', 'vacc_rabies']

class CatSerializer(AnimalSerializer):
    class Meta:
        model = Cat
        fields = ['name','breed', 'age', 'gender', 'fur_color', 'castrated', 'description', 'deficiencies','test_fiv', 'test_felv', 'vacc_rabies', 'vacc_poli']

class ProfileSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Profile
        fields = ['user', 'address_lat', 'address_long', 'cpf', 'phone_number']

class AdopterSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Adopter
        fields = ['user', 'address_lat', 'address_long', 'cpf', 'phone_number', 'house_type', 'window_nets', 'kids']

class MessageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ChatMessage
        fields = ['receiver_id','sender_id']
