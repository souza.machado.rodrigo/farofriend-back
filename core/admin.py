from django.contrib import admin
import core.models as models

# Register your models here.
admin.site.register(models.Animal)
admin.site.register(models.Dog)
admin.site.register(models.Cat)
admin.site.register(models.Profile)
admin.site.register(models.Adopter)
admin.site.register(models.ChatMessage)
