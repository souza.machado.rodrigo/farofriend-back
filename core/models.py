from django.db import models
from django.contrib.auth.models import User

class Animal(models.Model):
    name = models.CharField(max_length=50)
    breed = models.CharField(max_length=50)
    age = models.IntegerField(blank=True, null=True)
    gender = models.CharField(max_length=20)
    fur_color = models.CharField(max_length=50)
    castrated = models.BooleanField()
    description = models.TextField(blank=True, null=True)
    deficiencies = models.TextField()
    images = models.ImageField(blank=True, null=True)

    def __str__(self):
         return self.name

class Dog(Animal):
    size = models.CharField(max_length=20)
    test_distemper = models.CharField(max_length=20)
    vacc_rabies = models.BooleanField()

    def __str__(self):
         return self.name

class Cat(Animal):
    test_fiv = models.CharField(max_length=20)
    test_felv = models.CharField(max_length=20)
    vacc_rabies = models.BooleanField()
    vacc_poli = models.BooleanField()

    def __str__(self):
         return self.name

class Profile(models.Model):
    user = models.OneToOneField(User, related_name='profile', on_delete=models.CASCADE)
    address_lat = models.CharField(max_length=30)
    address_long = models.CharField(max_length=30)
    cpf = models.CharField(max_length=11)
    phone_number = models.CharField(max_length=15, blank=True, null=True)
    animals = models.ManyToManyField(Animal, blank=True)

    def __str__(self):
         return f"{self.user.first_name} {self.user.last_name}"

class Adopter(Profile):
    house_type = models.CharField(max_length=30)
    window_nets = models.BooleanField()
    kids = models.BooleanField()

    def __str__(self):
         return f"{self.user.first_name} {self.user.last_name}"

class ChatMessage(models.Model):
    sender = models.ForeignKey(User, related_name="sender", on_delete=models.CASCADE)
    receiver = models.ForeignKey(User, related_name="receiver", on_delete=models.CASCADE)
    message = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
         return f"{self.sender.first_name} -> {self.receiver.first_name}: {self.timestamp.date()} {self.timestamp.time()}"

