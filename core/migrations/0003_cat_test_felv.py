# Generated by Django 4.1.3 on 2022-11-12 21:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_alter_chatmessage_sender'),
    ]

    operations = [
        migrations.AddField(
            model_name='cat',
            name='test_felv',
            field=models.CharField(default='', max_length=20),
            preserve_default=False,
        ),
    ]
