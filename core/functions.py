from django.contrib.auth.models import User
from core.models import Profile
import math, operator

def get_profile(username):
    user = User.objects.get(username=username)
    profile = Profile.objects.get(user=user)

    return profile

def distance_between_points(profile_a, profile_b):
    lat_a = float(profile_a.address_lat)
    long_a = float(profile_a.address_long)
    lat_b = float(profile_b.address_lat)
    long_b = float(profile_b.address_long)

    return math.sqrt(math.pow(lat_b-lat_a, 2)+math.pow(long_b-long_a, 2))

def get_sorted_profiles(request):
    self_profile = Profile.objects.get(user=request.user)
    profiles = Profile.objects.all().exclude(user=request.user)
    for profile in profiles:
        profile.distance = distance_between_points(self_profile, profile)

    sorted_profiles = sorted(profiles, key=operator.attrgetter('distance'))

    return sorted_profiles