from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from django.contrib.auth.models import User
from django.db.models import Q

from core.models import ChatMessage, Profile
from core import functions
from itertools import chain
from datetime import datetime

class MessageView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        messages = ChatMessage.objects.filter(Q(sender=request.user) | Q(receiver=request.user)).order_by('timestamp')
        msg_list = []

        for message in messages:
            msg_list.append({
                "sender": message.sender.username,
                "receiver": message.receiver.username,
                "message": message.message,
                "timestamp": message.timestamp
            })

        return Response(msg_list, 200)

    def post(self, request):
        receiver = request.data['receiver']
        message = request.data['message']

        sender_user = request.user
        receiver_user = User.objects.get(username=receiver)

        message_obj = ChatMessage.objects.create(
            sender = sender_user,
            receiver = receiver_user,
            message = message,
            timestamp = datetime.now()
        )

        message_obj.save()

        return Response("message sent", 200)

class ListAdpotionAnimalsView(APIView):
    def get(self,request):
        profiles = functions.get_sorted_profiles(request)
        animals = []
        for profile in profiles:
            for animal in profile.animals.all().values():
                animals.append(animal)

        return Response(animals,200)
